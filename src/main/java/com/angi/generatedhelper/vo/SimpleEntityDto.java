
package com.angi.generatedhelper.vo;

public class SimpleEntityDto {

  private Integer entityId = null;
  private String entityHash = null;

  public SimpleEntityDto (Integer entityId, String entityHash) {
    this.entityId = entityId;
    this.entityHash = entityHash;
  }

  public Integer getEntityId() {
    return entityId;
  }
  public void setEntityId(Integer entityId) {
    this.entityId = entityId;
  }

  public String getEntityHash() {
    return entityHash;
  }

  public void setEntityHash(String entityHash) {
    this.entityHash = entityHash;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SimpleEntityDto {\n");
    sb.append("    entityId: ").append(String.valueOf(entityId)).append("\n");
    sb.append("    entityHash: ").append(entityHash).append("\n");
    sb.append("}");
    return sb.toString();
  }

}
